import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BotaoGrandeComponent } from './botao-grande/botao-grande.component';

@NgModule({
  imports: [
    CommonModule // CommonModule possui as diretivas básicas, ngIf, ngFor
  ],
  declarations: [BotaoGrandeComponent],
  exports: [BotaoGrandeComponent] // exporta um componente do modulo para que outros modulos possa usá-lo
})
export class BotoesModule { }
