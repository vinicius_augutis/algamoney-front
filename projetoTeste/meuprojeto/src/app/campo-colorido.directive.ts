import { Directive, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appCampoColorido]',
  exportAs: 'campoColorido'
})
export class CampoColoridoDirective {


  @Input('appCampoColorido') cor = 'gray';
  @HostBinding('style.backgroundColor') corDefundo: string; // alterar propriedade do elemento com o valor do corDeFundo

  constructor(
  ) {}

  @HostListener('focus') colorir() { // HotListener responde a eventos do template para a diretiva
    this.corDefundo = this.cor;
  }

  @HostListener('blur') descolorir() {
    this.corDefundo = 'transparent';
  }

}
