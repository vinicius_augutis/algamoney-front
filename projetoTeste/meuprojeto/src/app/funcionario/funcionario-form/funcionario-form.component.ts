import { FuncionarioService } from './../funcionario.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-funcionario-form',
  templateUrl: './funcionario-form.component.html',
  styleUrls: ['./funcionario-form.component.css'],
  providers: [FuncionarioService]
  // colocando isso aqui ele cria uma outra instancia de funcionarioService para esse componente e todos seus filhos.
  // Se eu retirar ele, volta a funcionar porque dai o APPComponent e ele utilizam a mesma instância
  // assim ele cria uma outra instância que é diferente do appModule e vai ter escopos diferentes. Com injetor filho
})
export class FuncionarioFormComponent {
  ultimoId = 0;
  nome = 'Thiago';
  adicionado = false;
  @Output() funcionarioAdicionado = new EventEmitter();

  constructor(private funcionarioService: FuncionarioService) {


  }
  adicionar(nome) {
    this.funcionarioService.adicionar(nome);
  }
}
