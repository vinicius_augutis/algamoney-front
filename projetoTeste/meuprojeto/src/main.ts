import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

// Aqui eu indico para o angular iniciar a aplicação e carregar o módulo principal que é o appModule
// Bootstrap da aplicação vai ser iniciado por esse módulo (Boostrap é inicialização)
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
