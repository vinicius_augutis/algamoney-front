import { LogoutService } from './logout-service';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { MoneyHttp } from './money-http';
import { Http } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from 'primeng/components/button/button';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';

import { SegurancaRoutingModule } from './seguranca-routing.module';
import { LoginFormComponent } from './login-form/login-form.component';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { RequestOptions } from '@angular/http';

export function authHttpServiceFactory(auth: AuthService, http: Http, options: RequestOptions) {
  const config = new AuthConfig({
    globalHeaders: [
      { 'Content-Type': 'application/json' }
      // isso aqui configura o contentType para json para todas requisições e não preciso passar header em cada requisição
    ]
  });

  return new MoneyHttp(auth, config, http, options);
  // com isso retorno no http service sendo o moneyHttp que é o que criei para interceptar as chamadas http
  // Indiretamente vamos continuar usando o AuthHttp e o token vai funcionar normal, porque o MoneyHttp
  // extends do AuthHttp. Assim usamos polimorfismo
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    InputTextModule,
    ButtonModule,

    SegurancaRoutingModule
  ],
  declarations: [LoginFormComponent],
  providers: [
    {provide: AuthHttp,
    useFactory: authHttpServiceFactory,
    deps: [AuthService, Http, RequestOptions] // passa uma instancia do HTTP e uma instancia do RequestOptions para o useFactory
    },
    AuthGuard,
    LogoutService
  ]
})
export class SegurancaModule { }
